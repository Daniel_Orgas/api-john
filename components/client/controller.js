const store = require('./store');

function addClient(name){
    if(!name){
        return Promise.reject('Invalid name');
    }
    
    const user = {
        name,
    };

    return store.add(user);

}

function getClient(){
    // return new Promise((resolve, reject) => {
    //     resolve(store.get());

    // });
    console.log("hello")
    return store.get();
    
}

module.exports = {
    addClient,
    getClient,
};

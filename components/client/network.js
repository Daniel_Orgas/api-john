const express = require ('express');
const router = express.Router();
const controller = require('./controller');
const response = require('../../network/response');

router.post('/', function(req, res){
    controller.addClient(req.body.name)
    .then(data => {
        response.success(req, res,data, 200);
    })
    .catch(err =>{
        response.error(req, res,'Internal error', 500, err);
    });
});

router.get('/', (req, res) =>{
    
    controller.getClient()
    .then(clientList=>{
        response.success(req,res,clientList,200);
    })
    .catch( e =>{

        response.error(req, res, 'Unexpected Error',500, e);

    });

});

module.exports = router;

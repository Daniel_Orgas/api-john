const config = {

    //dbUrl: process.env.DB_URL || 'mongodb+srv://db_user_rs:ZAM7M7OGdwhXFukj@cluster0-kgiuv.mongodb.net/telegrom?retryWrites=true&w=majority',
    port: process.env.PORT || 1321,
    host: process.env.HOST || 'http://localhost',
    publicRoute: process.env.PUBLIC_ROUTE || '/app',
    filesRoute: process.env.FILES_ROUTE || 'files',

};

module.exports = config;

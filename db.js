const db = require('mongoose');

db.Promise = global.Promise;

async function connect(url){

    await db.connect(url,{
    userNewUrlParser: true,//hacemos que no haya ni un problema de compatibilidad 
    useUnifiedTopology: true,
    });
    console.log('[db] conectada con exito');

}

module.exports = connect;
